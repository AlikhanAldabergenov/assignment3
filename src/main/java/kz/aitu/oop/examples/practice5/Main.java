package kz.aitu.oop.examples.practice5;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Main {
    public static void main(String[] args) {
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/stones","root","");
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery("select * from precious");
            double totalW = 0;
            int totalC = 0;
            while(rs.next())
            {
                System.out.println(rs.getInt(1)+"  "+rs.getString(2)+"  "+rs.getString(3));
                totalW += rs.getDouble(5);
                totalC += rs.getInt(4);
            }
            System.out.println();
            System.out.println("Total weight is: " + totalW + " and total cost is: " + totalC);
            con.close();
        }
        catch(Exception e)
        {
            System.out.println(e);
        }
    }
}
