package kz.aitu.oop.examples.practice7;

public class FoodFactory {

    public static Food getFood(String getType) {
        if (getType == null) {
            return null;
        }
        if (getType.equalsIgnoreCase("Dessert")) {
            return new Pizza();
        }
        else if (getType.equalsIgnoreCase("Fast Food")) {
            return new Cake();
        }

        return null;
    }
}
