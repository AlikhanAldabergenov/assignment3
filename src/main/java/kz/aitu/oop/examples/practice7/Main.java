package kz.aitu.oop.examples.practice7;

public class Main {
    public static void main(String[] args) {
        FoodFactory foodFactory = new FoodFactory();
        Food food1 = foodFactory.getFood("Dessert");
        food1.cook();
        Food food2 = foodFactory.getFood("Fast Food");
        food2.cook();
    }
}
