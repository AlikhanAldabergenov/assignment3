package kz.aitu.oop.examples.practice2;

public class RailwayCarriage {
    private String name;
    private int weight;

    public RailwayCarriage(String n, int w) {
        this.setWeight(w);
        this.setName(n);
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public int getWeight(){
        return weight;
    }

    public void setWeight(int weight){
        this.weight = weight;
    }
}
