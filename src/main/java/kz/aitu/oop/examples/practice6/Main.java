package kz.aitu.oop.examples.practice6;

public class Main {
    public static void main(String[] args) {
        Singleton singleton1 = Singleton.getSingleInstance();
        Singleton singleton2 = Singleton.getSingleInstance();
        singleton1.str = "hello world";
        singleton2.str = "Hello I am a Singleton! Let me say hello world to you";
        System.out.println(singleton1.str);
        System.out.println(singleton2.str);
    }
}
