package kz.aitu.oop.examples.practice6;

public final class Singleton {
    static Singleton instance = null;
    public String str;

    private Singleton() {}

    public static Singleton getSingleInstance() {
        if (instance == null) {
            instance = new Singleton();
        }
        return instance;
    }
}
