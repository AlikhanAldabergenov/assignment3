package kz.aitu.oop.examples.practice3;

public class Employees {
    private String name;
    private int cost;

    public Employees(String n,int c) {
        this.setCost(c);
        this.setName(n);
    }
    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }
}

