package kz.aitu.oop.examples.assignment3;

public class MyString {
    int[] array;

    public MyString(int[] values){
        this.array=values;
    }
    public int length(){
        return array.length;
    }
    public int valueAt(int position){
        if(position<0 || position >= array.length){
            return -1;
        }
        return array[position];
    }
    public boolean contains(int value){
        for(int i=0; i < array.length; i++){
            if(array[i] == value){
                return true;
            }
        }
        return false;
    }
    public int count(int value){
        int count = 0;
        for(int i=0; i < array.length; i++){
            if(array[i] == value){
                count++;
            }
        }
        return count;
    }
    public void print(){
        for(int i=0; i < array.length; i++){
            System.out.println(array[i]+" ");
        }
    }
}
