package kz.aitu.oop.examples.practice4;

public class Fish {
    private String name;
    private int cost;

    public Fish(String n,int c) {
        this.setName(n);
        this.setCost(c);
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }
}
